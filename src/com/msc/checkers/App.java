package com.msc.checkers;

import com.msc.checkers.entity.Board;

public class App
{

    public static void main(String[] args)
    {
        
        System.out.println("-----------------------------------------");
        System.out.println("  Welcome to the Java Checkers Game    ");
        System.out.println("-----------------------------------------\n");
        
        Board board = new Board();
        board.draw();

    }

}
