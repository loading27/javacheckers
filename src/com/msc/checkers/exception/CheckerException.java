package com.msc.checkers.exception;

public abstract class CheckerException extends Exception
{
    private static final long serialVersionUID = 2588436247299513114L;

    public CheckerException(String msg)
    {
        super(msg);
    }

}
