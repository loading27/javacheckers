package com.msc.checkers.exception;

public class InvalidMovementException extends CheckerException
{
    private static final long serialVersionUID = -2609193692342071142L;

    public InvalidMovementException(String msg)
    {
        super(msg);
    }

}
