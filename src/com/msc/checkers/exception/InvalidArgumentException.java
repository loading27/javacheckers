package com.msc.checkers.exception;

public class InvalidArgumentException extends CheckerException
{
    private static final long serialVersionUID = -4549072385003898336L;

    public InvalidArgumentException(String msg)
    {
        super(msg);
        // TODO Auto-generated constructor stub
    }

}
