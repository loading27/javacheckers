package com.msc.checkers.service;

import java.util.List;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.KingPiece;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.service.internal.AvailableMovesService;

public class MovementServiceImpl implements MovementService
{
    public void movePiece(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {

        RefereeService refereeService = new ReferreServiceImpl();
        refereeService.validateMove(player, board, fromCell, toCell);

        // Everything is OK. Move piece.
        AvailableMovesService availableMovesService = new AvailableMovesService();
        List<Cell> mandatoryMoves = availableMovesService.getMandatoryMoves(player, board, fromCell, toCell);

        // move the piece to the new cell
        Cell bFromCell = board.getCell(fromCell);
        Cell bToCell = board.getCell(toCell);
        bToCell.setPiece(bFromCell.getPiece());
        // remove the piece from the old cell
        bFromCell.removePiece();

        if (!mandatoryMoves.isEmpty())
        {
            // the player will capture a piece.
            // Remove the captured piece from the board
            removeCapturedPiece(board, fromCell, toCell);
        }

        // Upgrade to KingPiece if applicable
        if (shouldBeKing(board, bToCell))
        {
            bToCell.setPiece(new KingPiece(bToCell.getPiece().getColor()));
        }

    }

    private void removeCapturedPiece(Board board, Cell fromCell, Cell toCell)
    {
        int initX, initY;
        int endX, endY;

        if (toCell.getRow() > fromCell.getRow())
        {
            // top to bottom movement
            initY = fromCell.getRow();
            endY = toCell.getRow();
        }
        else
        {
            // bottom to top movement
            initY = toCell.getRow();
            endY = fromCell.getRow();
        }

        if (toCell.getCol() > fromCell.getCol())
        {
            // left to right movement
            initX = fromCell.getCol();
            endX = toCell.getCol();
        }
        else
        {
            // right to left movement
            initX = toCell.getCol();
            endX = fromCell.getCol();
        }

        // Remove pieces
        for (int y = initY; y <= endY; y++)
        {
            for (int x = initX; x <= endX; x++)
            {
                Cell bCell = board.getCell(y, x);
                bCell.removePiece();
            }
        }
    }

    private boolean shouldBeKing(Board board, Cell cell)
    {
        return board.getNumberOfRows() == cell.getRow();
    }

}
