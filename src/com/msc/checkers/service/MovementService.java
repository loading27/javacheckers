package com.msc.checkers.service;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;

public interface MovementService
{
    public void movePiece(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException;
}
