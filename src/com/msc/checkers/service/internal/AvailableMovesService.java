package com.msc.checkers.service.internal;

import java.util.ArrayList;
import java.util.List;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.KingPiece;
import com.msc.checkers.entity.PieceColor;
import com.msc.checkers.entity.Player;

public class AvailableMovesService
{

    public List<Cell> getMandatoryMoves(Player player, Board board, Cell fromCell, Cell toCell)
    {
        List<Cell> mandatoryMoves = new ArrayList<Cell>();

        // check forward diagonals
        try
        {
            // top left cell
            Cell topLeft = board.getCell(fromCell.getRow() - 1, fromCell.getCol() - 1);
            if (topLeft.getPiece().getColor() != player.getColor())
            {
                // there is an opponent piece available. Check if it is possible to capture
                if (board.getCell(topLeft.getRow() - 1, topLeft.getCol() - 1).getPiece() == null)
                {
                    if (board.getCell(fromCell).getPiece() instanceof KingPiece
                                    || player.getColor() == PieceColor.BLACK)
                    {
                        mandatoryMoves.add(new Cell(topLeft.getRow() - 1, topLeft.getCol() - 1));
                    }
                }
            }
        }
        catch(NullPointerException npe)
        {
        }

        try
        {
            // top right cell
            Cell topRight = board.getCell(fromCell.getRow() - 1, fromCell.getCol() + 1);
            if (topRight.getPiece().getColor() != player.getColor())
            {
                // there is an opponent piece available. Check if it is possible to capture
                if (board.getCell(topRight.getRow() - 1, topRight.getCol() + 1).getPiece() == null)
                {
                    if (board.getCell(fromCell).getPiece() instanceof KingPiece
                                    || player.getColor() == PieceColor.BLACK)
                    {
                        mandatoryMoves.add(new Cell(topRight.getRow() - 1, topRight.getCol() + 1));
                    }
                }
            }
        }
        catch(NullPointerException npe)
        {
        }
        // check backward diagonals
        try
        {
            // back left cell
            Cell backLeft = board.getCell(fromCell.getRow() + 1, fromCell.getCol() - 1);
            if (backLeft.getPiece().getColor() != player.getColor())
            {
                // there is an opponent piece available. Check if it is possible to capture
                if (board.getCell(backLeft.getRow() + 1, backLeft.getCol() - 1).getPiece() == null)
                {
                    if (board.getCell(fromCell).getPiece() instanceof KingPiece || player.getColor() == PieceColor.RED)
                    {
                        mandatoryMoves.add(new Cell(backLeft.getRow() + 1, backLeft.getCol() - 1));
                    }
                }
            }
        }
        catch(NullPointerException npe)
        {
        }
        try
        {
            // top right cell
            Cell backRight = board.getCell(fromCell.getRow() + 1, fromCell.getCol() + 1);
            if (backRight.getPiece().getColor() != player.getColor())
            {
                // there is an opponent piece available. Check if it is possible to capture
                if (board.getCell(backRight.getRow() + 1, backRight.getCol() + 1).getPiece() == null)
                {
                    if (board.getCell(fromCell).getPiece() instanceof KingPiece || player.getColor() == PieceColor.RED)
                    {
                        mandatoryMoves.add(new Cell(backRight.getRow() + 1, backRight.getCol() + 1));
                    }
                }
            }
        }
        catch(NullPointerException npe)
        {

        }
        return mandatoryMoves;
    }
}
