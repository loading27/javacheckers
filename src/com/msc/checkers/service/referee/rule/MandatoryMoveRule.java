package com.msc.checkers.service.referee.rule;

import java.util.List;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.exception.InvalidArgumentException;
import com.msc.checkers.exception.InvalidMovementException;
import com.msc.checkers.service.internal.AvailableMovesService;

public class MandatoryMoveRule implements RefereeRule
{

    private Board board;
    private Player player;
    private Cell fromCell;
    private Cell toCell;

    @Override
    public void init(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {
        if (player == null)
            throw new InvalidArgumentException("Player is required to apply validation");

        if (board == null)
            throw new InvalidArgumentException("Board is required to apply validation");

        this.player = player;
        this.board = board;

        if (fromCell == null || toCell == null)
            throw new InvalidArgumentException("A fromCell and a toCell are required to apply validation");

        this.fromCell = fromCell;
        this.toCell = toCell;
    }

    @Override
    public void validate() throws CheckerException
    {
        AvailableMovesService availableMovesService = new AvailableMovesService();
        List<Cell> mandatoryMoves = availableMovesService.getMandatoryMoves(player, board, fromCell, toCell);
        if (!mandatoryMoves.isEmpty())
        {
            if (!mandatoryMoves.contains(toCell))
            {
                throw new InvalidMovementException("There are mandatory moves!");
            }
        }
    }

}
