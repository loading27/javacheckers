package com.msc.checkers.service.referee.rule;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;

public interface RefereeRule
{
    public void init(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException;

    public void validate() throws CheckerException;
}
