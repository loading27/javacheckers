package com.msc.checkers.service.referee.rule;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.exception.InvalidArgumentException;
import com.msc.checkers.exception.InvalidMovementException;

public class PlayerCanMovePieceRule implements RefereeRule
{

    private Board board;
    private Cell cell;
    private Player player;

    @Override
    public void init(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {
        if (player == null)
            throw new InvalidArgumentException("Player is required to apply validation");

        if (board == null)
            throw new InvalidArgumentException("Board is required to apply validation");

        this.player = player;
        this.board = board;

        if (fromCell == null)
        {
            if (toCell == null)
            {
                throw new InvalidArgumentException("A cell is required to apply validation");
            }
            else
            {
                this.cell = toCell;
            }
        }
        else
        {
            this.cell = fromCell;
        }
    }

    @Override
    public void validate() throws CheckerException
    {
        // Check if the piece belongs to the player that is trying to play
        if (player.getColor() != board.getCell(cell).getPiece().getColor())
        {
            throw new InvalidMovementException("Trying to move a piece from the opponent");
        }
    }

}
