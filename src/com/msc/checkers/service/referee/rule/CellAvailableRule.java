package com.msc.checkers.service.referee.rule;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.exception.InvalidArgumentException;
import com.msc.checkers.exception.InvalidMovementException;

public class CellAvailableRule implements RefereeRule
{

    private Board board;
    private Cell cell;

    @Override
    public void init(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {
        if (board == null)
            throw new InvalidArgumentException("Board is required to apply validation");

        this.board = board;

        if (fromCell == null)
        {
            if (toCell == null)
            {
                throw new InvalidArgumentException("A cell is required to apply validation");
            }
            else
            {
                this.cell = toCell;
            }
        }
        else
        {
            this.cell = fromCell;
        }
    }

    @Override
    public void validate() throws CheckerException
    {
        // Check if the cell is available to move the piece
        if (board.getCell(cell).getPiece() != null)
        {
            throw new InvalidMovementException("Cell already with piece");
        }
    }

}
