package com.msc.checkers.service.referee.rule;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.PieceColor;
import com.msc.checkers.entity.Player;
import com.msc.checkers.entity.SinglePiece;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.exception.InvalidArgumentException;
import com.msc.checkers.exception.InvalidMovementException;

public class MoveForwardRule implements RefereeRule
{

    private Board board;
    private Player player;
    private Cell fromCell;
    private Cell toCell;

    @Override
    public void init(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {
        if (player == null)
            throw new InvalidArgumentException("Player is required to apply validation");

        if (board == null)
            throw new InvalidArgumentException("Board is required to apply validation");

        this.player = player;
        this.board = board;

        if (fromCell == null || toCell == null)
            throw new InvalidArgumentException("A fromCell and a toCell are required to apply validation");

        this.fromCell = fromCell;
        this.toCell = toCell;
    }

    @Override
    public void validate() throws CheckerException
    {
        // Single pieces are only allowed to move forward
        if (board.getCell(fromCell).getPiece() instanceof SinglePiece)
        {
            if (!isForward(player, fromCell, toCell))
                throw new InvalidMovementException("Single pieces can only move forward");
        }
    }

    private boolean isForward(Player player, Cell fromCell, Cell toCell)
    {
        if (player.getColor() == PieceColor.BLACK)
        {
            // Black pieces are from bottom to top
            return toCell.getRow() < fromCell.getRow();
        }
        // Red pieces are from top to bottom
        return toCell.getRow() > fromCell.getRow();
    }

}
