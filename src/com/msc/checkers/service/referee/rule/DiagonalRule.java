package com.msc.checkers.service.referee.rule;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.exception.InvalidArgumentException;
import com.msc.checkers.exception.InvalidMovementException;

public class DiagonalRule implements RefereeRule
{

    private Cell fromCell;
    private Cell toCell;

    @Override
    public void init(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {
        if (fromCell == null || toCell == null)
            throw new InvalidArgumentException("A fromCell and a toCell are required to apply validation");

        this.fromCell = fromCell;
        this.toCell = toCell;
    }

    @Override
    public void validate() throws CheckerException
    {
        // Check if it is a diagonal movement
        if (!isDiagonal())
        {
            throw new InvalidMovementException("You are only allow to move pieces diagonally");
        }
    }

    private boolean isDiagonal()
    {
        // the abs between (from.y - to.y) should be equal to (from.x - to.x)
        return Math.abs(fromCell.getRow() - toCell.getRow()) == Math.abs(fromCell.getCol() - toCell.getCol());
    }

}
