package com.msc.checkers.service;

import com.msc.checkers.entity.Board;
import com.msc.checkers.entity.Cell;
import com.msc.checkers.entity.Player;
import com.msc.checkers.exception.CheckerException;
import com.msc.checkers.service.referee.rule.BoundariesRule;
import com.msc.checkers.service.referee.rule.CellAvailableRule;
import com.msc.checkers.service.referee.rule.DiagonalRule;
import com.msc.checkers.service.referee.rule.MandatoryMoveRule;
import com.msc.checkers.service.referee.rule.MoveForwardRule;
import com.msc.checkers.service.referee.rule.PieceAvailableRule;
import com.msc.checkers.service.referee.rule.PlayerCanMovePieceRule;
import com.msc.checkers.service.referee.rule.RefereeRule;
import com.msc.checkers.service.referee.rule.SingleJumpRule;
import com.msc.checkers.service.referee.rule.WhiteCellRule;

public class ReferreServiceImpl implements RefereeService
{

    @Override
    public void validateMove(Player player, Board board, Cell fromCell, Cell toCell) throws CheckerException
    {
        // SOURCE VALIDATIONS
        // Check if the fromCell is inside the board boundaries
        RefereeRule refRule = new BoundariesRule();
        refRule.init(player, board, fromCell, null);
        refRule.validate();

        refRule = new PieceAvailableRule();
        refRule.init(player, board, fromCell, null);
        refRule.validate();

        refRule = new PlayerCanMovePieceRule();
        refRule.init(player, board, fromCell, null);
        refRule.validate();

        // TARGET VALIDATIONS
        // Check if the targetCell is inside the board boundaries
        refRule = new BoundariesRule();
        refRule.init(player, board, null, toCell);
        refRule.validate();

        refRule = new WhiteCellRule();
        refRule.init(player, board, null, toCell);
        refRule.validate();

        refRule = new DiagonalRule();
        // TODO overload init method to avoid null parameters
        refRule.init(null, null, fromCell, toCell);
        refRule.validate();

        refRule = new MoveForwardRule();
        refRule.init(player, board, fromCell, toCell);
        refRule.validate();

        refRule = new MandatoryMoveRule();
        refRule.init(player, board, fromCell, toCell);
        refRule.validate();

        refRule = new SingleJumpRule();
        refRule.init(null, null, fromCell, toCell);
        refRule.validate();

        refRule = new CellAvailableRule();
        refRule.init(player, board, null, toCell);
        refRule.validate();
    }

}
