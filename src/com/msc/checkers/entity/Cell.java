/**
 * 
 */
package com.msc.checkers.entity;

import java.util.Objects;

/**
 * @author cunha
 *
 */
public class Cell
{
    private int row;
    private int col;
    private CellColor color;
    private Piece piece;

    public Cell(int row, int col, CellColor color, Piece piece)
    {
        this.row = row;
        this.col = col;
        this.color = color;
        this.piece = piece;
    }

    public Cell(int row, int col, CellColor color)
    {
        this(row, col, color, null);
    }

    public Cell(int row, int col)
    {
        this(row, col, null, null);
    }

    public int getRow()
    {
        return row;
    }

    public void setRow(int row)
    {
        this.row = row;
    }

    public int getCol()
    {
        return col;
    }

    public void setCol(int col)
    {
        this.col = col;
    }

    public CellColor getColor()
    {
        return color;
    }

    public void setColor(CellColor color)
    {
        this.color = color;
    }

    public Piece getPiece()
    {
        return piece;
    }

    public void setPiece(Piece piece)
    {
        this.piece = piece;
    }

    public void removePiece()
    {
        this.piece = null;
    }

    public void draw()
    {
        if (color == CellColor.WHITE)
        {
            System.out.print("    ");
            return;
        }
        if (piece != null)
        {
            piece.draw();
        }
        else
        {
            System.out.print(" -- ");
        }
    }

    @Override
    public boolean equals(Object o)
    {
        // If the object is compared with itself then return true
        if (o == this)
        {
            return true;
        }

        /*
         * Check if o is an instance of Cell or not "null instanceof [type]" also returns false
         */
        if (!(o instanceof Cell))
        {
            return false;
        }

        // typecast o to Complex so that we can compare data members
        Cell c = (Cell)o;

        // Compare the data members and return accordingly
        return row == c.getRow() && col == c.getCol();
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(row, col);
    }

}
