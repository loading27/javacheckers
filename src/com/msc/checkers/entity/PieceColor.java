/**
 * 
 */
package com.msc.checkers.entity;

/**
 * @author cunha
 *
 */
public enum PieceColor
{
    RED("R"), BLACK("B");

    private String color;

    PieceColor(String color)
    {
        this.color = color;
    }

    @Override
    public String toString()
    {
        return this.color;
    }

}
