package com.msc.checkers.entity;

public class Player
{

    private String username;
    private PieceColor color;

    public Player(String username, PieceColor color)
    {
        this.username = username;
        this.color = color;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public PieceColor getColor()
    {
        return color;
    }

    public void setColor(PieceColor color)
    {
        this.color = color;
    };

}
