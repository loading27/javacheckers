/**
 * 
 */
package com.msc.checkers.entity;

/**
 * @author cunha
 *
 */
public class Board
{
    private static int ROWS = 8;
    private static int COLS = 8;
    private static String CELL_DELIMITER = "|";
    private Cell[][] board;

    public Board()
    {
        this.board = new Cell[ROWS][COLS];
        init();
    }

    public void init()
    {
        for (int row = 0; row < ROWS; row++)
        {
            for (int col = 0; col < COLS; col++)
            {
                if (row % 2 == col % 2)
                {

                    board[row][col] = new Cell(row, col, CellColor.WHITE);

                }
                else
                {
                    board[row][col] = new Cell(row, col, CellColor.BLACK);
                    if (row < 3)
                    {
                        // At the beginning the cells in the first 3 rows contain red pieces
                        board[row][col] = new Cell(row, col, CellColor.BLACK, new SinglePiece(PieceColor.RED));
                    }
                    if (row >= ROWS - 3)
                    {
                        // At the beginning the cells in the last 3 rows contain black pieces
                        board[row][col] = new Cell(row, col, CellColor.BLACK, new SinglePiece(PieceColor.BLACK));
                    }
                }
            }
        }
    }

    public void draw()
    {
        drawHeader();
        for (int row = 0; row < ROWS; row++)
        {
            // print the row number
            System.out.print(row + 1);
            for (int col = 0; col < COLS; col++)
            {
                if (col == 0)
                    System.out.print(CELL_DELIMITER);
                board[row][col].draw();
                System.out.print(CELL_DELIMITER);
            }
            System.out.println();
        }
    }

    private void drawHeader()
    {
        System.out.println();
        for (int col = 0; col < COLS; col++)
        {
            if (col == 0)
            {
                System.out.print("  ");
            }
            // print the column number
            System.out.print(String.format("  %d\t", col + 1));
        }
        System.out.println();
    }

    public Cell getCell(int row, int col)
    {
        return board[row][col];
    }

    public Cell getCell(Cell cell)
    {
        return getCell(cell.getRow(), cell.getCol());
    }

    public int getNumberOfRows()
    {
        return ROWS;
    }

    public int getNumberOfColumns()
    {
        return COLS;
    }

}
