/**
 * 
 */
package com.msc.checkers.entity;

/**
 * @author cunha
 *
 */
public class KingPiece extends Piece
{

    public KingPiece(PieceColor color)
    {
        super(color);
    }

    @Override
    public void draw()
    {
        System.out.print(" " + color.toString() + "*" + " ");
    }

}
