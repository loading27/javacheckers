/**
 * 
 */
package com.msc.checkers.entity;

/**
 * @author cunha
 *
 */
public abstract class Piece
{
    protected PieceColor color;

    public Piece(PieceColor color)
    {
        this.color = color;
    }

    public PieceColor getColor()
    {
        return color;
    }

    public abstract void draw();

}
