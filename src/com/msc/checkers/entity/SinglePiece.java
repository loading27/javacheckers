/**
 * 
 */
package com.msc.checkers.entity;

/**
 * @author cunha
 *
 */
public class SinglePiece extends Piece
{

    public SinglePiece(PieceColor color)
    {
        super(color);
    }

    @Override
    public void draw()
    {
        System.out.print(" " + color + "  ");
    }

}
