/**
 * 
 */
package com.msc.checkers.entity;

/**
 * @author cunha
 *
 */
public enum CellColor
{
    WHITE, BLACK;

}
