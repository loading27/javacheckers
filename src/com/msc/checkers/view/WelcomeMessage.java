package com.msc.checkers.view;

public class WelcomeMessage
{

    public void display(){
        System.out.println("-----------------------------------------");
        System.out.println("  Welcome to the Java Checkers Game    ");
        System.out.println("-----------------------------------------\n");
    }
}
